# Maintainer: Jonathan Steel <jsteel at archlinux.org>
# Contributor: Benjamin Klettbach <b.klettbach@gmail.com>

pkgname=obs-studio
_pkgver='30.0.0'
# pkgver="$(tr '-' '.' <<< "$_pkgver" | tr -d '\n')"
pkgver=30.0.0.rc1.r75.35f848149
pkgrel=1
pkgdesc="Free, open source software for live streaming and recording"
arch=('x86_64')
url="https://obsproject.com"
license=('GPL2')
depends=('ffmpeg' 'jansson' 'libxinerama' 'libxkbcommon-x11' 'mbedtls' 'rnnoise' 'pciutils'
         'qt6-svg' 'curl' 'jack' 'gtk-update-icon-cache' 'pipewire' 'libxcomposite' 'onevpl')
makedepends=('cmake' 'libfdk-aac' 'x264' 'swig' 'python' 'luajit' 'sndio' 'ninja')
optdepends=('libfdk-aac: FDK AAC codec support'
            'libva-intel-driver: hardware encoding'
            'libva-mesa-driver: hardware encoding'
            'luajit: scripting support'
            'python: scripting support'
            'sndio: Sndio input client'
            'v4l2loopback-dkms: virtual camera support')
# "$pkgname::git+https://github.com/obsproject/obs-studio.git#tag=$_pkgver"
source=("$pkgname::git+https://github.com/obsproject/obs-studio.git#branch=master"
        fix_python_binary_loading.patch
        ignore_unused_submodules.patch
	0001-clang-workaround.patch
	0002-clang-workaround-2.patch
	0001-obs-ffmpeg-Add-AV1-support-for-VA-API.patch)
b2sums=('SKIP'
        '59819b54998cf7d4db63992c784aab69087c537bc62140bc7794d12bdda542b69ea9a48b856c0e6aaed223e1ee2648083389023ee112ca6597bbcf3a4e522833'
        'b614035b606b856c3b050f0563e18f0b69c714c31de7d94e394136fddf434a895a10e380e4dc0daf534d055828594cf0e7e144ddebdfd0f791705f0b3242869d'
        '344d672968cf8a116ab731d01aeaa7bc35d5270f34b15d328035f6348fca72d4c28f3ffd4b2acd82de58c9eac1bb4fd7ba335bcfcc60080173f8e2967607236f'
        'c546ab4b5334234256841717de87e7a104a362bfb55bc29ffda02a41c1c0d35ef32200b5fe27250d1eeb221ad5376603ff5349483444f6e070600c303862fe69'
        'b658ae806eebbb98a752044becddd6c4e7c7cb7d266f95531c4b14fcbca140e915f5f1bd14d67fed864f46c5fb3e6da367920640cdf996d032d49dc090c06ec8')
options=(debug)

pkgver() {
	cd "$pkgname"
	printf "%s" "$(git describe --long | sed 's/\([^-]*-\)g/r\1/;s/-/./g')"
}

prepare() {
  cd $pkgname
  local _f
  for _f in "${source[@]}"; do
    _f="$srcdir/$_f"
    grep -E '\.patch$' <<< "$_f" || continue
    msg2 "Applying patch $_f"
    patch -Np1 < "$_f"
  done
}

build() {
  # -DOBS_VERSION_OVERRIDE="$pkgver-$pkgrel" \
  cmake -B build -G Ninja -S $pkgname \
    -DCMAKE_INSTALL_PREFIX="/usr" \
    -DENABLE_BROWSER=OFF \
    -DENABLE_VST=ON \
    -DENABLE_VLC=OFF \
    -DENABLE_NEW_MPEGTS_OUTPUT=OFF \
    -DENABLE_AJA=OFF \
    -DENABLE_JACK=ON \
    -DENABLE_LIBFDK=ON \
    -DENABLE_WEBRTC=OFF \
    -DOS_LINUX=TRUE \
    -Wno-dev
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
}
